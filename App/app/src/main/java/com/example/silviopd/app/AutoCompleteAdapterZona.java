package com.example.silviopd.app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import com.example.silviopd.app.negocio.Zona;

import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapterZona extends ArrayAdapter<Zona> {

    public static List<Zona> listaZonas;


    public AutoCompleteAdapterZona(@NonNull Context context, @NonNull ArrayList<Zona> zonaList) {
        super(context, 0,  zonaList);

    }
}
