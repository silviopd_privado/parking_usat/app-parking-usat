package com.example.silviopd.app.negocio;

public class Acompanante {

    private String txtDni;
    private int txtCargo;
    private String txtZona;

    public String getTxtDni() {
        return txtDni;
    }

    public void setTxtDni(String txtDni) {
        this.txtDni = txtDni;
    }

    public int getTxtCargo() {
        return txtCargo;
    }

    public void setTxtCargo(int txtCargo) {
        this.txtCargo = txtCargo;
    }

    public String getTxtZona() {
        return txtZona;
    }

    public void setTxtZona(String txtZona) {
        this.txtZona = txtZona;
    }
}
