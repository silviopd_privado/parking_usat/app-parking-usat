package com.example.silviopd.app;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Marca;
import com.example.silviopd.app.negocio.Modelo;
import com.example.silviopd.app.negocio.TipoVehiculo;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogVehiculoFragment extends DialogFragment {

    Spinner cbotipovehiculodialog,cbomarcadialog,cbomodelodialog;
    Button btncancelardialog,btnaceptardialog;

    TCTipoVehiculo tareaTipoVehiculo;
    TCMarca tareaMarca;
    TCModelo tareaModelo;

    public DialogVehiculoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dialog_vehiculo, container, false);

        cbotipovehiculodialog = (Spinner)view.findViewById(R.id.cbotipovehiculodialog);
        cbomarcadialog = (Spinner)view.findViewById(R.id.cbomarcadialog);
        cbomodelodialog = (Spinner)view.findViewById(R.id.cbomodelodialog);
        btnaceptardialog = (Button) view.findViewById(R.id.btnaceptardialog);
        btncancelardialog = (Button) view.findViewById(R.id.btncancelardialog);

        btncancelardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        btnaceptardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("AFUERA","dfsf");
                sendBackResult();
            }
        });

        tareaTipoVehiculo = new TCTipoVehiculo();
        tareaTipoVehiculo.execute();

        tareaMarca= new TCMarca();
        tareaMarca.execute(1);

        tareaModelo= new TCModelo();
        tareaModelo.execute(1,4);

        return view;
    }

    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        DialogVehiculo listener = (DialogVehiculo) getTargetFragment();
        int tipo_vehiculo = TipoVehiculo.listaSerie.get(cbotipovehiculodialog.getSelectedItemPosition()).getId_tipo_vehiculo();
        int marca = Marca.listaSerie.get(cbomarcadialog.getSelectedItemPosition()).getId_marca();
        int modelo = Modelo.listaSerie.get(cbomodelodialog.getSelectedItemPosition()).getId_modelo();

        Log.e("TP",String.valueOf(tipo_vehiculo));
        Log.e("TP2",String.valueOf(marca));
        Log.e("TP3",String.valueOf(modelo));

        listener.DataDialogVehiculo(tipo_vehiculo,marca,modelo);
        dismiss();
    }


    public interface DialogVehiculo{
        void DataDialogVehiculo(int tipo_vehiculo,int marca,int modelo);
    }

    public class TCTipoVehiculo extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "tipo_vehiculo.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        TipoVehiculo.listaSerie.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            TipoVehiculo objSerie = new TipoVehiculo();
                            objSerie.setId_tipo_vehiculo(objItem.getInt("id_tipo_vehiculo"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            TipoVehiculo.listaSerie.add(objSerie);

                            arraySerries[i] = String.valueOf(objItem.getString("nombre"));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                arraySerries
                        );

                        cbotipovehiculodialog.setAdapter(adapter);

                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCMarca extends AsyncTask<Integer, Void, String> {

        @Override
        protected String doInBackground(Integer... params) {
            int id_tipo_vehiculo = params[0];

            String ws = Funciones.URL_WS+ "marca.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("id_tipo_vehiculo",String.valueOf(id_tipo_vehiculo));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        Marca.listaSerie.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Marca objSerie = new Marca();
                            objSerie.setId_tipo_vehiculo(objItem.getInt("id_tipo_vehiculo"));
                            objSerie.setId_marca(objItem.getInt("id_marca"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            Marca.listaSerie.add(objSerie);

                            arraySerries[i] = String.valueOf(objItem.getString("nombre"));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                arraySerries
                        );

                        cbomarcadialog.setAdapter(adapter);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCModelo extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            int id_tipo_vehiculo = (Integer) params[0];
            int id_marca = (Integer) params[1];

            String ws = Funciones.URL_WS+ "modelo.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("id_tipo_vehiculo",String.valueOf(id_tipo_vehiculo));
            parametros.put("id_marca",String.valueOf(id_marca));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        Modelo.listaSerie.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Modelo objSerie = new Modelo();
                            objSerie.setId_tipo_vehiculo(objItem.getInt("id_tipo_vehiculo"));
                            objSerie.setId_marca(objItem.getInt("id_marca"));
                            objSerie.setId_modelo(objItem.getInt("id_modelo"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            Modelo.listaSerie.add(objSerie);

                            arraySerries[i] = String.valueOf(objItem.getString("nombre"));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                arraySerries
                        );

                        cbomodelodialog.setAdapter(adapter);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
