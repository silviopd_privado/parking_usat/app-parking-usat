package com.example.silviopd.app.negocio;

import java.util.ArrayList;

public class TipoVehiculo {

    private int id_tipo_vehiculo;
    private String nombre;

    public static ArrayList<TipoVehiculo> listaSerie = new ArrayList<>();

    public int getId_tipo_vehiculo() {
        return id_tipo_vehiculo;
    }

    public void setId_tipo_vehiculo(int id_tipo_vehiculo) {
        this.id_tipo_vehiculo = id_tipo_vehiculo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
