package com.example.silviopd.app;


import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Cargo;
import com.example.silviopd.app.negocio.Zona;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class DialogPersonaFragment extends DialogFragment{

    Spinner cbocargodialog,cbozonadialog;
    Button btncancelardialog,btnaceptardialog;

    TCCargo tareaCargo;
    TCZona tareaZona;


    public DialogPersonaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dialog_persona, container, false);

        cbocargodialog = (Spinner)view.findViewById(R.id.cbocargodialog);
        cbozonadialog = (Spinner)view.findViewById(R.id.cbozonadialog);
        btnaceptardialog = (Button) view.findViewById(R.id.btnaceptardialog);
        btncancelardialog = (Button) view.findViewById(R.id.btncancelardialog);

        btncancelardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        tareaCargo = new TCCargo();
        tareaCargo.execute();

        tareaZona = new TCZona();
        tareaZona.execute();

        btnaceptardialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               sendBackResult();
            }
        });

        return view;
    }

    // Call this method to send the data back to the parent fragment
    public void sendBackResult() {
        // Notice the use of `getTargetFragment` which will be set when the dialog is displayed
        DialogPersona listener = (DialogPersona) getTargetFragment();
        listener.DataDialogPersona(Cargo.listaSerie.get(cbocargodialog.getSelectedItemPosition()).getId_cargo(),Zona.listaSerie.get(cbozonadialog.getSelectedItemPosition()).getId_zona());
        dismiss();
    }


    public interface DialogPersona{
        void DataDialogPersona(int cargo,String zona);
    }

    public class TCCargo extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "cargo.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        Cargo.listaSerie.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Cargo objSerie = new Cargo();
                            objSerie.setId_cargo(objItem.getInt("id_cargo"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            Cargo.listaSerie.add(objSerie);

                            arraySerries[i] = String.valueOf(objItem.getString("nombre"));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                arraySerries
                        );

                        cbocargodialog.setAdapter(adapter);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCZona extends AsyncTask<Void, Void, String>{

        @Override
        protected String doInBackground(Void... voids) {
            String ws = Funciones.URL_WS+ "zona.listar.reg.php";
            HashMap parametros = new HashMap<String,String>();
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONArray jsonArray = json.getJSONArray("datos");

                        Zona.listaSerie.clear();
                        String arraySerries[] = new String[jsonArray.length()];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject objItem = jsonArray.getJSONObject(i);
                            Zona objSerie = new Zona();
                            objSerie.setId_zona(objItem.getString("id_zona"));
                            objSerie.setNombre(objItem.getString("nombre"));
                            Zona.listaSerie.add(objSerie);

                            arraySerries[i] = String.valueOf(objItem.getString("nombre"));
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getActivity(),
                                android.R.layout.simple_spinner_dropdown_item,
                                arraySerries
                        );

                        cbozonadialog.setAdapter(adapter);
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
