package com.example.silviopd.app;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silviopd.app.negocio.Acompanante;
import com.example.silviopd.app.negocio.Marca;
import com.example.silviopd.app.util.Funciones;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrarFragment extends Fragment implements DialogAcompananteFragment.DialogAcompanante,DialogPersonaFragment.DialogPersona,DialogVehiculoFragment.DialogVehiculo{

    Button btnagregar_registrar;
    ListView lv_listado;

    AdaptadorAcompanante adaptador;
    ArrayList<Acompanante> listaClientes;

    Button btnbuscardni,btnbuscarnroplaca;

    EditText txtDni,txtMroPlaca;
    TCBuscarPersona tareaBuscarPersona;
    TCBuscarVehiculo tareaBuscarVehiculo;
    private int cargo;
    private String zona,dni;

    TextView txtcargopersona,txtzonapersona,txttipovehiculo,txtmarca,txtmodelo;

    public RegistrarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_registrar, container, false);

        btnagregar_registrar = (Button) view.findViewById(R.id.btnagregar_registrar);
        lv_listado = (ListView) view.findViewById(R.id.lv_acompanantes);

        txtcargopersona = (TextView) view.findViewById(R.id.txtcargo_registrar);
        txtzonapersona = (TextView) view.findViewById(R.id.txtzona_registrar);

        txttipovehiculo = (TextView) view.findViewById(R.id.txttipovehiculo_registrar);
        txtmarca = (TextView) view.findViewById(R.id.txtmarca_registrar);
        txtmodelo = (TextView) view.findViewById(R.id.txtmodelo_registrar);

        listaClientes = new ArrayList<Acompanante>();
        adaptador = new AdaptadorAcompanante(getContext(), listaClientes);
        lv_listado.setAdapter(adaptador);

        btnagregar_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAcompananteFragment dialog = new DialogAcompananteFragment();
                dialog.setTargetFragment(RegistrarFragment.this, 1);
                dialog.show(getFragmentManager(), "MyCustomDialog");
            }
        });

        btnbuscardni = (Button) view.findViewById(R.id.btnbuscardni);
        btnbuscarnroplaca = (Button) view.findViewById(R.id.btnbuscarnroplaca);

        txtDni = (EditText) view.findViewById(R.id.re_text);
        txtMroPlaca = (EditText) view.findViewById(R.id.re_text2);

        btnbuscardni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dni = txtDni.getText().toString();
                new TCBuscarPersona().execute(dni);
            }
        });

        btnbuscarnroplaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nro_placa = txtMroPlaca.getText().toString();
                new TCBuscarVehiculo().execute(nro_placa);
            }
        });

        return view;
    }



    @Override
    public void DataDialogPersona(int cargo, String zona) {
        if (DialogAcompananteFragment.res==1){
            Log.e("TP/MA/MO",String.valueOf(cargo)+" - "+String.valueOf(zona));
            txtcargopersona.setText(String.valueOf(cargo));
            txtzonapersona.setText(zona);
        }else{
            Acompanante obj=new Acompanante();
            obj.setTxtDni(dni);
            obj.setTxtCargo(cargo);
            obj.setTxtZona(zona);
            listaClientes.add(obj);
            adaptador.setListaDatos(listaClientes);
        }
    }

    @Override
    public void DataDialogVehiculo(int tipo_vehiculo, int marca, int modelo) {
        Log.e("TP/MA/MO",String.valueOf(tipo_vehiculo)+" - "+String.valueOf(marca)+" - "+String.valueOf(modelo));
        txttipovehiculo.setText(String.valueOf(tipo_vehiculo));
        txtmarca.setText(String.valueOf(marca));
        txtmodelo.setText(String.valueOf(modelo));
    }

    @Override
    public void DataDialogAcompanante(int res, String dni) {
        this.dni = dni;

        if (DialogAcompananteFragment.res ==1){
            DialogPersonaFragment dialog = new DialogPersonaFragment();
            dialog.setTargetFragment(RegistrarFragment.this, 1);
            dialog.show(getFragmentManager(), "DialogPersonaFragment");

        }else{
            Acompanante obj=new Acompanante();
            obj.setTxtDni(dni);
            obj.setTxtCargo(res);
            obj.setTxtZona("");
            listaClientes.add(obj);
            adaptador.setListaDatos(listaClientes);
        }
    }


    public class TCBuscarPersona extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            String dni = (String) params[0];

            String ws = Funciones.URL_WS+ "persona.buscar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("dni",String.valueOf(dni));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONObject jsonArray = json.getJSONObject("datos");

                        int res= jsonArray.getInt("resultado");
                        Log.e("DFSAFASDF",String.valueOf(res));

                        if (res==1){
                            DialogPersonaFragment dialog = new DialogPersonaFragment();
                            dialog.setTargetFragment(RegistrarFragment.this, 1);
                            dialog.show(getFragmentManager(), "DialogPersonaFragment");
                        }
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class TCBuscarVehiculo extends AsyncTask<Object, Void, String> {

        @Override
        protected String doInBackground(Object... params) {
            String nro_placa = (String) params[0];

            String ws = Funciones.URL_WS+ "vehiculo.buscar.php";
            HashMap parametros = new HashMap<String,String>();
            parametros.put("nro_placa",String.valueOf(nro_placa));
            String resultado = new Funciones().getHttpContent(ws, parametros);

            return resultado;
        }

        @Override
        protected void onPostExecute(final String resultado) {
            if ( ! resultado.isEmpty() ) {
                try {
                    JSONObject json = new JSONObject(resultado);
                    int estado = json.getInt("estado");
                    if (estado==200){
                        JSONObject jsonArray = json.getJSONObject("datos");

                        int res= jsonArray.getInt("resultado");
                        //Log.i("RESULTAAAAAAADO",String.valueOf(res));

                        if (res==1){
                            DialogVehiculoFragment dialog = new DialogVehiculoFragment();
                            dialog.setTargetFragment(RegistrarFragment.this, 1);
                            dialog.show(getFragmentManager(), "DialogVehiculoFragment");
                        }
                    }
                }catch (Exception e){
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
